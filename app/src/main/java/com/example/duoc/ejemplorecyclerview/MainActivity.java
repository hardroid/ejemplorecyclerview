package com.example.duoc.ejemplorecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.listaEjemplo);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new PeliculasAdapter(this, getValuesDummy());
        recyclerView.setAdapter(mAdapter);


          /*  mAdapter = new ComentarioAdapter(this, this.values.getComentarios());//CampingAdapter(this, this.values.getValues());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ListadoComentariosActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    //abrirDetalleCamping(position);
                }
            }));*/
        }


    private ArrayList<Pelicula> getValuesDummy(){

        ArrayList<Pelicula> valores = new ArrayList<>();
        for(int x = 0 ; x < 30 ; x++){
            Pelicula aux = new Pelicula();
            aux.setTitulo("Titulo " + x);
            aux.setDescripcion("Descripcion " + x);
            aux.setUrlImagen("http://avatarbox.net/avatars/img9/goku_thinkz_avatar_picture_24628.jpg");
            valores.add(aux);
        }
        return valores;

    }
}
