package com.example.duoc.ejemplorecyclerview;

/**
 * Created by Duoc on 11-05-2016.
 */
public class Pelicula {
    private String urlImagen;
    private String titulo;
    private String descripcion;

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
