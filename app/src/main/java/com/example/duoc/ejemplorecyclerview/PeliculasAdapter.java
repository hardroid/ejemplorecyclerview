package com.example.duoc.ejemplorecyclerview;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Duoc on 11-05-2016.
 */
public class PeliculasAdapter  extends  RecyclerView.Adapter<PeliculasAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<Pelicula> dataSet;

    public PeliculasAdapter(Context context, ArrayList<Pelicula> dataSet){
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_peliculas, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final Pelicula pelicula = dataSet.get(i);
        try {
            Picasso.with(context).load(pelicula.getUrlImagen()).into(viewHolder.imgPreview);

            viewHolder.txtTitulo.setText(pelicula.getTitulo());
            viewHolder.txtDescripcion.setText(pelicula.getDescripcion());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imgPreview;
        public TextView txtTitulo;
        public TextView txtDescripcion;

        public ViewHolder(View v) {
            super(v);
            //ButterKnife.bind(this, v);
            imgPreview = (ImageView)v.findViewById(R.id.imgPreview);
            txtTitulo = (TextView)v.findViewById(R.id.txtTitulo);
            txtDescripcion = (TextView)v.findViewById(R.id.txtDescripcion);
        }
    }
}
